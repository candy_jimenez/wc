package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

//wc -l : Prints the number of lines in a file.
//wc -w : prints the number of words in a file.
//wc -c : Displays the count of bytes in a file.
//wc -m : prints the count of characters from a file.
//wc -L : prints only the length of the longest line in a file.

func main() {
	args := os.Args[1:]
	commands, files := splitArgs(args)
	for _, file := range files {
		if fileExists(file) {
			if err := printResult(file, commands); err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

		} else {
			fmt.Printf("File %s does not exist\n", file)
		}
	}
}

func printResult(file string, commands []string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()
	result := count(f)
	//print default
	if len(commands) == 0 {
		fmt.Printf("\tWordCount: %d \tLineCount: %d \tByteCount: %d \tFile: %s \n", result.w, result.l, result.c, file)
	} else {
		for _, command := range commands {
			switch command {
			case "-l":
				fmt.Printf("\tLineCount: %d", result.l)
			case "-w":
				fmt.Printf("\tWordCount: %d ", result.w)
			case "-c":
				fmt.Printf("\tByteCount: %d", result.c)
			case "-m":
				fmt.Printf("\tCharCount: %d", result.m)
			case "-L":
				fmt.Printf("\tLength: %d", result.L)
			default:
				fmt.Printf("\tFlag not found: %s", command)
			}
		}
		fmt.Printf("\tFile: %s\n", file)
	}
	return nil
}

func splitArgs(args []string) ([]string, []string) {
	var files []string
	var commands []string
	for i := 0; i < len(args); i++ {
		arg := args[i]
		if strings.HasPrefix(arg, "-") {
			commands = append(commands, arg)
		} else {
			files = append(files, arg)
		}

	}
	return commands, files
}

func count(r io.Reader) WcResult {
	var result WcResult
	var charCount int
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		words := strings.Fields(scanner.Text())
		result.w += len(words)
		charCount = len(scanner.Text()) + 1
		result.m += charCount
		result.c += bytes.Count([]byte(scanner.Text()), []byte(""))
		if charCount > result.L {
			result.L = charCount
		}
		result.l++
	}
	return result
}

func fileExists(fileName string) bool {
	info, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// WcResult: test
type WcResult struct {
	l, w, c, m, L int
}
