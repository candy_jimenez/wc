package main

import (
	"io"
	"reflect"
	"strings"
	"testing"
)

func Test_fileExists(t *testing.T) {
	type args struct {
		fileName string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "File exist",
			args: args{
				fileName: "wc.go",
			},
			want: true,
		},
		{
			name: "File does not exist",
			args: args{
				fileName: "wc1.go",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := fileExists(tt.args.fileName); got != tt.want {
				t.Errorf("fileExists() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_count(t *testing.T) {
	type args struct {
		r io.Reader
	}
	tests := []struct {
		name string
		args args
		want WcResult
	}{
		{
			name: "Count success",
			args: args{
				r: strings.NewReader("testing sucks\n"),
			},
			want: WcResult{
				w: 2,
				l: 1,
				c: 14,
				m: 14,
				L: 14,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := count(tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("count() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_splitArgs(t *testing.T) {
	type args struct {
		args []string
	}
	tests := []struct {
		name  string
		args  args
		want  []string
		want1 []string
	}{
		{
			name: "Split Args Success",
			args: args{
				args: []string{"-l", "-m", "wc.go"},
			},
			want:  []string{"-l", "-m"},
			want1: []string{"wc.go"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := splitArgs(tt.args.args)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("splitArgs() got = %v, want %v", got, tt.want)
			}
			if !reflect.DeepEqual(got1, tt.want1) {
				t.Errorf("splitArgs() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_printResult(t *testing.T) {
	type args struct {
		file     string
		commands []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Print result Pass",
			args: args{
				file:     "wc.go",
				commands: []string{"-w", "-c", "-m", "-l", "-L"},
			},
			wantErr: false,
		},
		{
			name: "Print default flags Pass",
			args: args{
				file:     "wc.go",
				commands: []string{},
			},
			wantErr: false,
		},
		{
			name: "Print error flags ",
			args: args{
				file:     "wc.go",
				commands: []string{"-b"},
			},
			wantErr: false,
		},
		{
			name: "Print result fail",
			args: args{
				file:     "wc1.go",
				commands: []string{"-w"},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := printResult(tt.args.file, tt.args.commands); (err != nil) != tt.wantErr {
				t.Errorf("printResult() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
